const { promises: fs } = require('fs');
const service = require('./index');
const debug = require('debug')('xmlreport-reader:cli');

(async () => {
  debug('Reading of XML file started');
  const fileContents = await fs.readFile(`${__dirname}/${process.argv[2]}`, 'utf8')
    .catch((err) => {
      debug('Error reading file', err);
    });
  debug('Reading of XML file finished');

  // Parse XML
  const report = await service.parseReport(fileContents);

  console.log(JSON.stringify(report, null, 2));
})()
.then(() => {
  debug('CLI has finished successfully');
  process.exit(0);
})
.catch((err) => {
  debug('CLI failed with error', err);
  console.error(err);
  process.exit(1);
});
