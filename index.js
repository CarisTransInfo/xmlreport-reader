const parseReportModule = require('./lib/parseReport.js');
const version = require('./package.json').version;

module.exports = {
  parseReport: parseReportModule(version),
  version: version
}
