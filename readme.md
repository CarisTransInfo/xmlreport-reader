# XML Report Reader

[![NPM Version](https://img.shields.io/npm/v/@carisls/xmlreport-reader.svg?style=flat)](https://www.npmjs.org/package/@carisls/xmlreport-reader)
[![NPM Downloads](https://img.shields.io/npm/dm/@carisls/xmlreport-reader.svg?style=flat)](https://npmcharts.com/compare/@carisls/xmlreport-reader?minimal=true)
[![Install Size](https://packagephobia.now.sh/badge?p=%40carisls%2Fxmlreport-reader)](https://packagephobia.now.sh/result?p=%40carisls%2Fxmlreport-reader)
[![CircleCI](https://circleci.com/bb/CarisTransInfo/xmlreport-reader.svg?style=shield&circle-token=9703781686c516163623a46b64d89fe434304dda)](https://circleci.com/bb/CarisTransInfo/xmlreport-reader)

The purpose of this component is reading of standard XML documents with genomics data
created by Caris Life Sciences.

## Usage
```javascript
const xmlReader = require('@carisls/xmlreport-reader');

const { promises: fs } = require('fs');

(async() => {
  // Get XML file contents
  const fileContents = await fs.readFile(__dirname + '/myfile.xml', 'utf8');

  // Parse XML into Report
  const report = xmlReader(fileContents);

  // Prettify report JSON
  const reportString = JSON.stringify(report, null, 2);

  // Write to console
  console.log(reportString);

})()
  .catch((err) => {
    console.error(err);
  });

```

## Supported elements
Only few elements are currently supported for extraction:
- Cancer-Relevant Biomarkers
- Genomic Signatures
- Immunohistochemistry (IHC)
- Human Leukocyte Antigen (HLA)
- Available Clinical Trials
- References (supporting treatments)
