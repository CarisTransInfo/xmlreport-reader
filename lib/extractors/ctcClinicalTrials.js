module.exports = (debug) => (xml) => {
  debug('CTC', 'Processing started');
  const source = xml.root.report?.[0].ctcClinicalTrialDrugClassification;

  if(!source) {
    debug('CTC', 'Data not available');
    return undefined;
  }

  try {
    const result = [];

    for(const g of source) {
      const group = { group: g.drugClassification[0].trim(), trials: [] };

      for(const t of g.ctcClinicalTrial) {
        group.trials.push({
          drugGroup: t.ctcTrialProtocol?.[0].drugGroup?.[0],
          drugAgents: t.ctcTrialProtocol?.[0].drugAgents?.[0],
          criterion: {
            drugBiomarker: t.ctcTrialProtocol?.[0].ctcTrialProtocolCriterion?.[0].drugBiomarker?.[0],
            assayName: t.ctcTrialProtocol?.[0].ctcTrialProtocolCriterion?.[0].assayName?.[0],
            analyte: t.ctcTrialProtocol?.[0].ctcTrialProtocolCriterion?.[0].analyte?.[0]
          }
        });
      }

      result.push(group);
    }

    debug('CTC', 'Processing finished');
    return result;
  }
  catch(err) {
    debug('CTC Error', err);
    return undefined;
  }
};
