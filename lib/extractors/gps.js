module.exports = (debug) => (xml) => {
  debug('GPS', 'Processing started');
  const source = xml.root.report[0].mdcResult;
  if(!source) {
    debug('GPS', 'Data not available');
    return undefined;
  }

  try {
    debug('GPS', 'Processing finished');
    return source.map(i => ({
      organ: i.organ?.[0],
      value: parseInt(i.similarityScore?.[0])
    }));
  }
  catch(err) {
    debug('GPS', err);
    return undefined;
  }
}
