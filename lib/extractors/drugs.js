module.exports = (debug) => (xml) => {
  debug('Drugs', 'Processing started');
  const source = xml.root.report[0].highImpactResult;
  if(!source) {
    debug('Drugs', 'Data not available');
    return undefined;
  }

  try {
    debug('Drugs', 'Processing finished');
    return source.map(i => ({
      name: i.highImpactDrugs?.[0],
      biomarker: i.biomarkerName?.[0],
      method: i.method?.[0],
      analyte: i.analyte?.[0],
      result:i.result1?.[0],
      level: i.level?.[0],
      clinicalAssociation: i.clinicalAssociation?.[0] === '0' ? 'benefit' : 'lack of benefit'
    }));
  }
  catch(err) {
    debug('Drugs', err);
    return undefined;
  }
}
