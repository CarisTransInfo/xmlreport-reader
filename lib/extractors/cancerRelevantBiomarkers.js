module.exports = (debug) => (xml) => {
  debug('CancerRelevantBiomarkers', 'Processing started');
  const source = xml.root.report?.[0].lineageRelevantTests?.[0];
  if (!source) {
    debug('CancerRelevantBiomarkers', 'Data not available');
    return undefined;
  }

  try {
    const relevant = source.lineageRelevantTestsResults?.[0].result?.map((i) => ({
      biomarker: i.biomarker?.[0],
      method: i.method?.[0],
      analyte: i.analyte?.[0],
      result: i.result?.[0].trim(),
      relevant: true
    })) || [];

    const nonRelevant = source.nonLineageRelevantTestsResults?.[0].result?.map((i) => ({
      biomarker: i.biomarker?.[0],
      method: i.method?.[0],
      analyte: i.analyte?.[0],
      result: i.result?.[0].trim(),
      relevant: false
    })) || [];

    const result = [...relevant, ...nonRelevant];
    if(result.length) {
      debug('CancerRelevantBiomarkers', 'Processing finished');
      return result;
    }
    else {
      debug('CancerRelevantBiomarkers', 'Data not available');
      return undefined;
    }
  }
  catch(err) {
    debug('CancerRelevantBiomarkers', err);
    return undefined;
  }
}
