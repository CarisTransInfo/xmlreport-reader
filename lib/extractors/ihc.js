module.exports = (debug) => (xml) => {
  debug('IHC', 'Processing started');
  const source = xml.root.report[0].ihcResultsTestSet[0]?.ihcResultsTest?.[0]?.result;
  if(!source) {
    debug('IHC', 'Data not available');
    return undefined;
  }

  try {
    debug('IHC', 'Processing finished');
    return source.map((i) => {
      const r = {
        biomarker: i.gene?.[0],
        stainingIntensity: parseInt(i.stainingIntensity?.[0]) || 0,
        percentStaining: parseInt(i.percentStaining?.[0]) || null,
        result: i.reportDisplayValue?.[0]
      }
      r.stainingIntensity = r.stainingIntensity > 0
        ? `${r.stainingIntensity}+`
        : r.stainingIntensity.toString();
      return r;
    });
  }
  catch(err) {
    debug('IHC', err);
    return undefined;
  }
}
