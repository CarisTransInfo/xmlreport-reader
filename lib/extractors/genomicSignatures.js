module.exports = (debug) => (xml) => {
  debug('GenomicSignatures', 'Processing started');
  const source = xml.root.report[0].genomicSignature;
  if (!source) {
    debug('GenomicSignatures', 'Data not available');
    return undefined;
  }

  try {
    const signatures = {};

    for(const item of source) {
      if(item.biomarker[0].match(/MSI/i)) {
        signatures.msi = {
          biomarker: item.biomarker?.[0],
          method: item.method?.[0],
          analyte: item.analyte?.[0],
          result: item.result?.[0].trim()
        }
      }
      else if (item.biomarker[0].match(/TMB/i)) {
        signatures.tmb = {
          biomarker: item.biomarker?.[0],
          method: item.method?.[0],
          analyte: item.analyte?.[0],
          result: item.result?.[0].trim(),
          value: !isNaN(item.proteinChange?.[0]) ? parseInt(item.proteinChange?.[0]) : null
        }
      }
      else if (item.biomarker[0].match(/LOH/i)) {
        signatures.loh = {
          biomarker: item.biomarker?.[0],
          method: item.method?.[0],
          analyte: item.analyte?.[0],
          result: item.result?.[0].trim(),
          value: !isNaN(item.dnaChange?.[0]) ? parseInt(item.dnaChange?.[0]) : null
        }
      }
    }

    debug('GenomicSignatures', 'Processing finished');
    return signatures;
  }
  catch(err) {
    debug('GenomicSignatures', err);
    return undefined;
  }
};
