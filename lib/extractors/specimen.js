module.exports = (debug) => (xml) => {
  const source = xml.root.FirstHeader?.[0];
  if(!source) {
    debug('Specimen', 'Data not available');
    return undefined;
  }

  try {
    debug('Specimen', 'Processing finished');
    return {
      id: source.specimenid?.[0],
      specimenSite: source.specimenSite?.[0],
      primaryTumorSite: source.primaryTumorSite?.[0],
      collectedOn: source.specimenCollectionDate?.[0]?.replace(/(\d+)\/(\d+)\/(\d+)/, '$3-$1-$2')
    }
  }
  catch(err) {
    debug('Specimen', err);
    return undefined;
  }

}