module.exports = (debug) => (xml) => {
  debug('FOLFOX', 'Processing started');
  const source = xml.root.report[0].FOLFOXPredictorResult?.[0];
  if(!source) {
    debug('FOLFOX', 'Data not available');
    return undefined;
  }

  try {
    debug('FOLFOX', 'Processing finished');
    return {
      classification: source.classification?.[0],
      detail: source.detail?.[0]
    }
  }
  catch(err) {
    debug('FOLFOX', err);
    return undefined;
  }
}
