module.exports = (debug) => (xml) => {
  debug('References', 'Processing started');
  const source = xml.root.appendix[0].ruleReference;
  if(!source) {
    debug('References', 'Data not available');
    return undefined;
  }

  try {
    source.sort((a, b) => a.ref[0] < b.ref[0] ? -1 : a.ref[0] > b.ref[0] ? 1 : 0);

    debug('References', 'Processing finished');
    return source.map(i=> ({
      title: i.title[0],
      url: i.url[0],
      drugs: i.drugs[0].drug,
      biomarkers: i.biomarkers[0].biomarker
    }));
  }
  catch(err) {
    debug('References', err);
    return undefined;
  }
}
