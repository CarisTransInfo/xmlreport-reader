module.exports = (debug) => (xml) => {
  debug('Genes', 'Processing started');
  const sourceP = xml.root.report?.[0].ngsAltTestSet?.[0].ngsAltTest;
  const sourceU = xml.root.report?.[0].ngsVusAltTestSet?.[0].ngsAltTest;

  if(!sourceP && !sourceU) {
    debug('Genes', 'Data not available');
    return undefined;
  }

  function extractGene (i) {
    return {
      biomarker: i.gene?.[0],
      method: i.method?.[0],
      analyte: i.analyte?.[0],
      result: i.result?.[0].trim(),
      proteinChange: i.alteration?.[0],
      dnaChange: i.dnaChange?.[0],
      exon: i.exon?.[0].trim(),
      variantFrequency: parseInt(i.mutated?.[0]) || null,
      interpretation: i.interpretation?.[0].trim(),
      summary: i.summary?.[0].trim()
    };
  }

  let genesP;
  let genesU;

  try {
    if(sourceP?.length) {
      debug('Genes', 'Extracting pathogenic genes started');
      genesP = sourceP.map(extractGene);
      debug('Genes', 'Extracting pathogenic genes finished');
    }
    else {
      debug('Genes', 'Pathogenic genes source not available');
      genesP = [];
    }

    if(sourceU?.length) {
      debug('Genes', 'Extracting uncertain genes started');
      genesU = sourceU.map(extractGene);
      debug('Genes', 'Extracting uncertain genes finished');
    }
    else {
      debug('Genes', 'Uncertain genes source not available');
      genesU = [];
    }
  }
  catch(err) {
    debug('Genes', err);
    return undefined;
  }

  debug('GenesPathogenic', 'Processing finished');
  return [...genesP, ...genesU];
}