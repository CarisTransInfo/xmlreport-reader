module.exports = (debug) => (xml) => {
  debug('HLA', 'Processing started');
  const source = xml.root.report[0].wtsHLATestSet?.[0].wtsHLAClassification?.[0];
  if(!source) {
    debug('HLA', 'Data not available');
    return undefined;
  }

  try {
    debug('HLA', 'Processing finished');
    return {
      classification: source.classification,
      items: source.wtsHLATest.map(i=>({
        biomarker: i.gene?.[0],
        method: i.method?.[0],
        analyte: i.analyte?.[0],
        result: i.genotype?.[0].trim()
      }))
    };
  }
  catch(err) {
    debug('HLA', err);
    return undefined;
  }
};
