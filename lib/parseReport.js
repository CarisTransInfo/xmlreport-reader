const xml2js = require('xml2js');
const debug = require('debug')('xmlreport-reader');

const specimenModule = require('./extractors/specimen')(debug);
const drugsModule = require('./extractors/drugs')(debug);
const ihcModule = require('./extractors/ihc')(debug);
const genomicSignaturesModule = require('./extractors/genomicSignatures')(debug);
const cancerRelevantModule = require('./extractors/cancerRelevantBiomarkers')(debug);
const genesModule = require('./extractors/genes')(debug);
const referencesModule = require('./extractors/references')(debug);
const hlaTestsModule = require('./extractors/hlaTests')(debug);
const ctcTrialsModule = require('./extractors/ctcClinicalTrials')(debug);
const gpsModule = require('./extractors/gps')(debug);
const folfoxModule = require('./extractors/folfox')(debug);

module.exports = (version) => async (fileContents) => {
  if(!fileContents) {
    debug('Empty data, skipping');
    return null;
  }

  const parser = new xml2js.Parser();
  debug('Parsing XML contents into JS');
  const obj = await parser.parseStringPromise(fileContents);

  debug('Parsing Report');
  return {
    version,
    specimen: specimenModule(obj),
    drugs: drugsModule(obj),
    folfox: folfoxModule(obj),
    cancerRelevantBiomarkers: cancerRelevantModule(obj),
    genomicSignatures: genomicSignaturesModule(obj),
    genes: genesModule(obj),
    hla: hlaTestsModule(obj),
    ihc: ihcModule(obj),
    gps: gpsModule(obj),
    ctcTrials: ctcTrialsModule(obj),
    references: referencesModule(obj),
  };
}
